import json
import boto3

device = '<DEVICE_NAME>'
topic = device + "/reboot"

client = boto3.client('iot-data', region_name='<AWS_REGION>')

def lambda_handler(event, context):
    response = client.publish(
        topic=topic,
        qos=0,
        payload=json.dumps({"reboot":True})
    )
    return '''<center><a class="btn btn-primary">   <DEVICE_NAME>   </a><cwdb-action action="call" endpoint="<LAMBDA_FUNCTION_ARN>"></cwdb-action> </center>'''

