# Setting up CloudWatch Dashboard

## Introduction
This document provides the step-by-step procedure for creating a Cloudwatch Dashboard.

To create the Cloudwatch Dashboard you will need to go through the following steps:
- Create a new Dashboard on Cloudwatch.
- Creating graph widgets for detections.
- Creating custom widgets for reboot button.
- Creating Gauge widgets for system monitoring.
- Creating alarm.
- Creating a text widget.
- Rearranging and resizing of the widgets.

## Create a new Dashboard on Cloudwatch
- Open the AWS console and login as root user.
- On the search bar, search for **Cloudwatch** and access this service.
- On the Cloudwatch console page, on the left side menu, go to **Dashboards**.
- Click on **Create dashboard** and give a name of your choice for it.

## Creating graph widgets for detections
After the creation of a dashboard, you have the possibility to add widgets from a pop up window. Otherwise, go to the **Dashboards** and click on the recently created dashboard.
- From the pop-up window, or after clicking the right-hand corner **+** button of the dashboard page, select the **Line** type of graph as a new widget.
- Select **Metrics**.
- On the **Browse** column, select the namespace. Select the metrics you want to add.

    Note that if detection metrics have not been pushed from the edge
    at least once then we will not see the name of the device in the
    namespace column. To push those metrics, deploy your app by following the use case setup.
- On the **Graphed metrics** column:
    - Under **Period** dropdown menu, select **1 second**.
    - We can customize the color code for the device by clicking on the color icon.
    - When satisfied, click on **Create widget** button.
- On the dashboard, we can select the time range of all the metrics on the top right side.
- Click the "Save" button.

## Creating custom widgets for reboot button
To create a reboot button, we have to create a lambda function for it.
- Go to the AWS console and search **Lambda** and go to the lambda console.
- On the console, go to **Functions** and click on **Create function**:
    - Select **Author from scratch**.
    - In **Basic information**, give the function a name.
    - Select the **Runtime** as **Python 3.9**.
    - Select the architecture as **arm64**.
    - Click on the **Create function** button.
- On the local system, open the lambda function located in <PROJECT_ROOT>/Cloudwatch/lambda.py in an editor.
    - Change the device name at line number 4 and 15.
    - Update the AWS region in line number 7.
    - On the AWS **lambda function console**.
        - Click on the lambda function created and copy the ARN.
    - Paste the copied ARN in lambda.py script at line number 15 in field <LAMBDA_FUNCTION_ARN>.
    - Save the script.
    - Rename the function to lambda_function.py
        ```sh
            mv lambda.py lambda_function.py
        ```
- zip the lambda function using the command below.
    ```sh
    zip -r lambda.zip lambda_function.py
    ```
- On the AWS **lambda function console**, on the lambda function created:
    -  Click on **Upload from** dropdown menu and select **.zip file**. Upload the zipped lambda function. Click on **Save**.

        Note that you may need to copy this zipped file from the edge to your host device. Or you can directly use the AWS CLI.
    - On the **Configuration** tab.
        - Click on **Permission** on the left side menu.
        - Under **Execution role**, click on the **Role name** provided.
            - Into **Permissions** tab, from the **Add permissions** dropdown menu select **Attach policies**.
            - Click on **Create policy** and select the **JSON** tab.
            - Replace the default text with the following to allow all the IoT actions and resources.
                ```
                {
                    "Version": "2012-10-17",
                        "Statement": [
                        {
                        "Effect": "Allow",
                            "Action": [
                                "iot:*"
                            ],
                            "Resource": [
                                "*"
                            ]
                        }
                    ]
                }
                ```
            - Click on **Next** and again on **Next** to review the policy.
            - Give the policy a name and click on **Create policy**.
            - Back on the role of the lamda function, attach the newly created policy.
    - Click on **Actions** dropdown menu and select **Publish new version**.
    - On version description field, write **1**.
- Go to the **CloudWatch** console and access the dashboard previously created.
    - Click on the **+** button to access **Add widget** menu.
    - Select **Custom widget**.
    - Do not select any template and click on **Next**.
    - Select the lambda function which we created and give the version as "1".
    - Click on **Preview widget** and if prompted, click on **Allow always**.

        You should be able to see a button in the preview window.
    - Under **Widget options**, in **Update one** section, uncheck **Refresh**.
    - Click on **Create widget**.
    - Save the dashboard.

        Note that after creating the widget, we need to create a service on the Edge device for the device to reboot.

- We can verify if the reboot button is working correctly or not by going into **AWS IoT** console and clicking onto **MQTT test client** in the left side menu.
    - Under **Subscribing to a topic**, write by format of “<device>/reboot”. Click on **Subscribe**.
    - Under **Subscribtions**, you should see the subscripted topic.
    - On the dashboard created, if we click on the reboot button, we should see a message “reboot: True” getting published on the subscribed topic.


## Creating Gauge widgets for system monitoring
- Open the **Cloudwatch** console and go to the dashboards created.
- Access the add widget menu through the **+** button.
- Select **Gauge**.
- Select a namespace, a metric with number of dimensions. Select the CPU or memory utilization metric.
- On the **Graphed metrics** tab, select the **Statistic** as **Maximum** and select the **Period** as **1 second**.
- On the **Options** tab:
    - tick the **Live data** option.
    - fill the **Min** and **Max** of **Gauge range** section.

        If we have selected CPU usage, the range would be 0-100 and for memory usage the range would be 0 – maximum device RAM memory in MB.
- Click on **Create widget** and we should be able to see a gauge widget on our dashboard.
- Save the dashboard.

## Creating alarm
For showing the device status, we need to create an alarm on the Cloudwatch console and add it to our dashboard.

- Go to the CloudWatch console. Under **Alarms**, select **All alarms**.
- Select **Create alarm**. Then, click on **Select metric**.
    - Select a namespace, click on metrics with number dimensions.
    - Select the metrics name related to the device status.
    - Once the metric selected, click on **Select metric**.
- Back on the **Create alarm** page. Select **Statistic** as **Maximum** and **Period** as **1 minute**.
- Under **Conditions** section:
    - Select **Static** and **Lower**.
    - Define the threshold value as "1".
- Click on **Next**.
- On the next window, click on the **Remove** button under the notification section. Click on **Next**.
- Give the alarm a name and click on **Next**.
- Click on **Create alarm**.
- Select the alarm which we have created.
- On the top right, in the **Actions** dropdown menu, select **Add to dashboard**.
    - Select the dashboard and give a custom widget name.
    - Under **Widget type**, select the type as **Number**.
    - Click on **Add to dashboard**.
- Go to the dashboard which we are editing and we should see a new
widget has been created.
- On the alarm widget, select the three dots and in **Sparkline**, select **Show sparkline**.
- We can now see the timeline from which the device was online.
- Save the dashboard.

## Creating a text widget
- Go to the dashboard created and click on the button to add a widget. Select **Text** widget.

    We can see how the widget works by referring to the predefined text on the creation console.
- Remove everything from **Markdown** section and paste the following to get the headline.
    ```
    # ARM-CSP Traffic Management Dashboard
    ```
- To get a smaller headline for differentiating widgets we use the following format.
    ```
    ## <Secondary headline>
    ```

    We can see how the widget looks in the preview window while editing.
- After editing click on **Create widget**.
- Save the dashboard.

## Rearranging and resizing of the widgets
For rearranging the widgets, hover the mouse cursor on the top of the widget. We should see the cursor icon changing. Then, by holding the left mouse button down we can move the widget.

For resizing the widgets, hover the cursor to the right bottom of the widget and we should see the cursor changing into a resize icon. Then, by holding the left mouse button down we can resize the widget.

- After all the editing save the dashboard.

While opening the dashboard, we need to turn off the reboot service on the edge device or else the device will also reboot. Because when opening the dashboard, the Cloudwatch refreshes all the widgets and thus executes the reboot lambda function which triggers the reboot action.
