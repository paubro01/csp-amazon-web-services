from awscrt import mqtt
from uuid import uuid4
import os
import json
import argparse
from awscrt import io, http, auth
from awsiot import mqtt_connection_builder


rootCAPath = "<DEVICE_CERTIFICATES_FOLDER_PATH>/root.ca.pem"                                                     
certificatePath = "<DEVICE_CERTIFICATES_FOLDER_PATH>/device.pem.crt"                                             
privateKeyPath = "<DEVICE_CERTIFICATES_FOLDER_PATH>/device-private.pem.key"                                      
host = "<AWS_IOT_ENDPOINT_URL>"                                
port = 8883                                                                             
clientId = "<MQTT_ID>" # Should be different from clinet id from Cloud container                                                                     
reb_topic = "<DEVICE_NAME>/reboot"
pod_name = "pod"


class MQTT_builder:
    def __init__(self, description) -> None:
        self.parser = argparse.ArgumentParser(description="Send and receive messages through and MQTT connection.")
        self.commands = {}
        self.parsed_commands = None

    def build_mqtt_connection(self, on_connection_interrupted, on_connection_resumed, endpoint, port, cert_path, pri_key, ca_path, client_id):
        mqtt_connection = mqtt_connection_builder.mtls_from_path(
            endpoint=endpoint,
            port=port,
            cert_filepath=cert_path,
            pri_key_filepath=pri_key,
            ca_filepath=ca_path,
            on_connection_interrupted=on_connection_interrupted,
            on_connection_resumed=on_connection_resumed,
            client_id=client_id,
            clean_session=False,
            keep_alive_secs=30,
            http_proxy_options=None)
        return mqtt_connection

def reboot_callback(topic, payload, dup, qos, retain, **kwargs):                                                                                                          
    print(type(payload.decode("utf-8")))
    command = str(payload.decode("utf-8")).strip("\n{}][").replace(" ", "").split(":")                                                                                                        
    print("received command:", command)
    mqtt_connection.disconnect()                                                                                                               
    if command[1][1:-1]=="True" or command[1]=="true":                                                                                                                             
        print("Rebooting the device")      
        os.system("kubectl delete pod " + pod_name)                                                                                                           
        os.system("reboot") 

mqtt_builder = MQTT_builder("Basic Connect - Make a MQTT connection.")

def on_connection_interrupted(connection, error, **kwargs):                             
    print("Connection interrupted. error: {}".format(error))                            
def on_connection_resumed(connection, return_code, session_present, **kwargs):          
    print("Connection resumed. return_code: {} session_present: {}".format(return_code, session_present))
                                                                                                         
mqtt_connection = mqtt_builder.build_mqtt_connection(on_connection_interrupted, on_connection_resumed, host, port, certificatePath, privateKeyPath, rootCAPath, clientId)
connect_future = mqtt_connection.connect()                                                                                                            
connect_future.result()                                                                                                                               
print("Connected to AWS IOT as MQTT client!")                                                                                                         

subscribe_future, packet_id = mqtt_connection.subscribe(topic=reb_topic, qos=mqtt.QoS.AT_MOST_ONCE, callback=reboot_callback)
subscribe_result = subscribe_future.result()
print("topic ", reb_topic, "subscribed")

while True:
    pass
