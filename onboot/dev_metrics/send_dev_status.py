import psutil
import boto3
import json
import os
import subprocess
import time


cloudwatch = boto3.client("cloudwatch", region_name='<AWS_REGION>',
                                        aws_access_key_id='<AWS_ACCESS_KEY>',
                                        aws_secret_access_key='<AWS_SECRET_ACCESS_KEY>' )

cw_namespace = "<DEVICE_NAME>"
cloudwatch_post_interval = 60

def get_pod_status(pod_name):
    usg = {}
    try:
        result = str(subprocess.check_output("kubectl describe PodMetrics "+pod_name, shell=True)).replace(" ", "").split("\\n")
        
        res_lst = [i.split(":") for i in result]
        cleaned = []
        for i in res_lst:
            if i[0] == "Name":
                cleaned.append(i)
            elif i[0] == "Cpu":
                i[1] = (int(i[1][:-1]) / (1000000000 * 6)) * 100 # nanocore to percentage
                cleaned.append(i)
            elif i[0] == "Memory":
                i[1] = int(i[1][:-2]) / 977 #convert kibibyte to MB
                cleaned.append(i)
        for i in range(0, len(cleaned), 3):
            usg[cleaned[i][1]] = dict(cleaned[i+1:i+3])
    except Exception as e:
        print("No containers are running.....")
    if len(usg) == 1:
        if usg.keys()[0] == "preprocess":
            usg["inference"] = {"Cpu":0, "Memory":0}
        elif usg.keys()[0] == "inference":
            usg["preprocess"] = {"Cpu":0, "Memory":0}
    elif len(usg) == 0:
        usg["inference"] = {"Cpu":0, "Memory":0}
        usg["preprocess"] = {"Cpu":0, "Memory":0}
    return usg

#___________SENDING_KUBERNETES_CONTAINER_STATS_TO_CLOUDWATCH___________________
#while True:
#    pod_usage = get_pod_status("pod")
#    print(pod_usage)
#    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Device_status", "Unit":"None", "Value": 1} ], Namespace="RK3399")
#    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Prepro_Mem(MB)", "Unit":"None", "Value": pod_usage["preprocess"]["Memory"]} ], Namespace="RK3399")
#    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Prepro_Cpu(Nanocore)", "Unit":"None", "Value": pod_usage["preprocess"]["Cpu"]} ], Namespace="RK3399")
#    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "inf_Mem(MB)", "Unit":"None", "Value": pod_usage["inference"]["Memory"]} ], Namespace="RK3399")
#    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "inf_Cpu(Nanocore)", "Unit":"None", "Value": pod_usage["inference"]["Cpu"]} ], Namespace="RK3399")


#___________SENDING_DEVICE_STATS_TO_CLOUDWATCH________________________________
def get_stats():
    mem_used = psutil.virtual_memory().used / (1024 * 1024)
    cpu_per = psutil.cpu_percent()
    return cpu_per,  int(mem_used)

while True:
    cpu_usage, mem_usage = get_stats()
    print(cpu_usage, mem_usage)
    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Device_status", "Unit":"None", "Value": 1} ], Namespace=cw_namespace)
    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Cpu Usage", "Unit":"None", "Value": cpu_usage} ], Namespace=cw_namespace)
    cloudwatch.put_metric_data(MetricData = [ {"MetricName": "Memory usage", "Unit":"None", "Value": mem_usage} ], Namespace=cw_namespace)
    time.sleep(cloudwatch_post_interval)
