# Setting up Services on Edge

## Introduction
This document guides the user for setting up required services to push device metrics to AWS Cloudwatch and reboot functionalities and on start deployment of the Kubernetes pod.

## Steps Overview
1. Installing the required packages.
2. Setting up the services.
    - Setting up reboot service.
    - Setting up device status service.
    - Setting up kubernetes deployment service.

## Installing the required packages
- Open a terminal on the host system and connect to the Edge device using SSH.
- Install the required packages for the services.
```sh
pip install awsiot awsiotsdk psutil awscrt
```

## Setting up the services
- On the edge device, go to the onboot folder.
```sh
cd <WORKSPACE>/onboot
```

### Setting up the reboot service
- Change the directory to reboot folder.
```sh
cd reboot/
```
- Copy the AWS IoT certificates folder to the current working directory.

    Refer to the README of the Greengrass folder for downloading the certificates.

```sh
cp -r <path_to_device_certs>/ .
```
- Open the **reboot_sub.py** file in an editor. Update:
    - The <DEVICE_CERTIFICATES_FOLDER_PATH> with the path from reboot folder to the device certification folder.
    - The <AWS_IOT_ENDPOINT_URL>

        Note that this URL can be found by accessing the AWS **IoT Core** console. Go to **Settings** on the left side menu. Copy the URL under the **Device data endpoint** section.
    - The <DEVICE_NAME>.
    - The <MQTT_ID>.

        Note that the MQTT id can be mentioned according to the user's choice. This should be different from the client id mentioned in config.json of the preprocess container.
    - Save the file.
- Open the **reboot.service** file in an editor.
    - Under **ExecStart** field, update the values with the path to the python executable and with the absolute path to **reboot_sub.py** file.
    - Save the file.
- Copy the **reboot.service** file to /etc/systemd/system/ folder.
```sh
cp reboot.service /etc/systemd/system/
```
- Reload the systemctl daemon.
```sh
systemctl daemon-reload
```
- Execute the below command to enable the reboot service to start on booting of the device.
```sh
systemctl enable reboot
```
- Start the service using command below.
```sh
systemctl start reboot
```
- Check the status of the service using the command below.
```sh
systemctl status reboot
```

The status of the service should be as active.

Useful commands related to services:
- To view the stdout and stderr of a systemd unit use the following.
    ```sh
    journalctl -eu <service_name>
    ```
    Option:
    - -e is to see the end of the file.
    - -u to specify the unit.

### Setting up the device status service
From the onboot/ folder:
- Navigate to dev_metrics folder on terminal.
```sh
cd dev_metrics/
```
- Open the **send_dev_status.py** file in an editor. Update:
    - The AWS region <AWS_REGION>.
    - The AWS access key <AWS_ACCESS_KEY>.
    - The AWS secret access key <AWS_SECRET_ACCESS_KEY>.
    - The device name <DEVICE_NAME>.
    - Save the file.
- Open the **send_dev_status.service** file in an program editor.
    - Under **ExecStart** field, update the path to python executable path and with the absolute path to **send_dev_status.py**
    - Save the file.
- Copy the service file to **/etc/systemd/system/** folder:
```sh
cp send_dev_status.service /etc/systemd/system/
```
- Reload the systemctl daemon.
```sh
systemctl daemon-reload
```
- Execute the below command to enable the send_dev_status service to start on booting of the device.
```sh
systemctl enable send_dev_status
```
- Start the service using command below.
```sh
systemctl start send_dev_status
```
- Check the status of the service using the command below.
```sh
systemctl status send_dev_status
```
The status of the service should be as active.

### Setting up the kubernetes deployment service
Before setting up kubernetes service, you need to first set up the kubernetes pod using pod file. Please refer to the AWS/kubernetes/README document for deployment of containers using kubernetes pod.
- Change the directory to services folder located at "onboot/".
```sh
cd <PROJECT_ROOT_FOLDER>/onboot/kubernetes_service/
```
- Open the service file with respect to the type of deployment chosen in a editor.
    - Update the path to the corresponding pod file and save the file.

        Note, you may need to update the path to kubectl executable.
    - Save file.
- Copy the service file to /etc/systemd/system/ folder.
```sh
cp <DEPLOYMENT_SERVICE_FILE> /etc/systemd/system/
```
- Reload the systemctl daemon.
```sh
systemctl daemon-reload
```
- Execute the below command to enable the kubernetes service to start on booting of the device.
```sh
systemctl enable <DEPLOYMENT_SERVICE>
```
- Start the service using command below.
```sh
systemctl start <DEPLOYMENT_SERVICE>
```
- Check the status of the service using the command below.
```sh
systemctl status <DEPLOYMENT_SERVICE>
```

The status of the service should be as active.
Note that if the kubernetes is already configured for deployment the service will not be active.