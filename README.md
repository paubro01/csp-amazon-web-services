# Setup of AWS Cloud for ARM-CSP architecture

## Introduction
This document provides the overall steps to enable AWS cloud services for the application microservices. It is recommended to first have a view and a try with the application microservices available at **Traffic Monitoring/Application Microservices**.

## Clone the repository
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
Follow the below sequence to set up the services and containers.

## Setup the Greengrass core service on Edge
Follow the steps described in the README of the Greengrass folder for:
  - The setup of the Greengrass core service.
  - The download of the device certificates which will enable us to send data to AWS IoT core service. These certificates will be used to build the Cloud container.

## Building the Cloud container
Follow the steps described in the README of the Cloud_container folder for building and deploying the csp container.

## Setup the Cloudwatch dashboard.
Follow the steps described in the README of the Cloudwatch folder for setting up the cloudwatch dashboard.

**Note** that before following this document, we need to run the inference pipeline at least once to push the metrics to the cloudwatch service.

## Deploying all the containers using kubernetes
Follow the steps described in the kubernets folder's ReadMe for deploying all the containers using kubernetes.

## Setup the services on Edge device
Follow the steps described in the README of the onboot folder for setting up the services.

**Note** that Kubernetes service setup should be done after completing deployment of all the containers using kubernetes.

