# Setting up Greengrass and related AWS services

## Introduction
This document provides a step by step guide to set up AWS IoT Greengrass v2 service on Edge device.

## Structure of the document
### Steps Overview
1. Creating a default deployment for Greengrass on AWS IoT Core.
2. Building the Docker container for Greengrass.
3. Configure the env file and credentials file.
4. Run the Greengrass container.
5. Copy the lambda function and launch it inside the container.
6. Download certificates for device.
7. Save the container for future use.
8. Push the container to Docker hub.

### Help and references
1. Issues with IAM role.
2. Issues with Greengrass Deployment.
3. References.

## Creating a default deployment for Greengrass on AWS IoT Core
First, we need to access the AWS console. Then follow the steps below:

1. Search and select **IoT Core** service from search results of the top search bar.
2. In the left side menu under **All devices** click on **Thing groups**.
    - Click on **Create thing group**.
    - Select **Create static thing group** and click on **Next**.
    - Enter a **Thing group name** of user choice.
    - Keep all the other options as default and click on **Create thing group**.
3. On the left side menu under **Security** select **Policies**.
    - Click on **Create policy**.
    - Provide a name of user choice for the policy.
    - In **Policy action** dropdown select "\*" and in **Policy resource** enter "\*".
    - Click on **Create**.
4. On the left side menu under **Greengrass devices** select **Deployments**.
    - Click on **Create**.
    - Provide a name of user choice for deployment.
    - For the deployment target, select **Thing group**. Under the dropdown menu, select the thing
    group which was created previously and click on **Next**.
    - Under **Public components** search bar, look for  and select each of the below components to add them to the deployment.
        - aws.greengrass.Cli
        - aws.greengrass.LambdaLauncher
        - aws.greengrass.LambdaManager
        - aws.greengrass.LambdaRuntimes
        - aws.greengrass.LocalDebugConsole
        - aws.greengrass.Nucleus
    - After selecting all the components click on **Next**.
    - In this window we need to configure the version of the components:
        - Select **aws.greengrass.Cli** and click on **Configure component**.
        - In the **Component version** dropdown at top, select **Version: 2.5.3** and click on **Confirm** at bottom.
        - Select **aws.greengrass.Nucleus** and click on **Configure component**.
        - In the **Component version** dropdown at top select **Version: 2.5.3** and click on **Confirm** at bottom.
        - After changing the versions of those two components, click on **Next**.
    - On this page, click on **Next**.
    - On the **Review** page, verify the information and click on **Deploy**. This will deploy the configuration to the thing group.

        Note that this configuration can be used for multiple devices. Mention the thing group name in .env file while building and running the Greengrass container.

## Building the docker container for Greengrass
- Open a terminal on the host system and connect to the Edge device using SSH.
- Download the Greengrass repository from github.
```sh
wget https://github.com/aws-greengrass/aws-greengrass-docker/archive/refs/heads/main.zip
```
- Extract the zip file and go to the extracted greengrass folder.
```sh
unzip main.zip
cd aws-greengrass-docker-main
```
- In the **Dockerfile** file, under **ENV** command section, paste the following and update the values:
```
PROVISION=true
AWS_REGION=<AWS_REGION>
THING_NAME=<NAME_OF_THE_EDGE_DEVICE>
THING_GROUP_NAME=<THING_GROUP_CREATED_PREVIOUSLY>
```
- Build the docker container.
    ```sh
    docker build –t <IMAGE_NAME>:<TAG_NAME> .
    ```
    Mention the image name & tag name of your choice.

## Configure the env file and credentials file
To run the docker image we need to configure one env file and a credentials file which has the AWS credentials.
- Go to the workspace folder and create one file with the name “gg.env” with the following contents:
    ```
    GGC_ROOT_PATH=/greengrass/v2
    AWS_REGION=<Region>
    PROVISION=true
    THING_NAME=<Device_name>
    THING_GROUP_NAME=<Thing_group_created_previously>
    TES_ROLE_NAME=GreengrassV2TokenExchangeRole
    TES_ROLE_ALIAS_NAME=GreengrassCoreTokenExchangeRoleAlia
    COMPONENT_DEFAULT_USER=ggc_user:ggc_group
    ```
    - Update the <AWS_REGION>, <THING_GROUP_NAME> and <THING_NAME> fields. The device name is according to user preference. You will need to reuse it in further steps.
    - Save the file.
- Create another file as “credentials” and paste the following contents in that file:
    ```
    [default]
    aws_access_key_id = <AWS_ACCESS_KEY>
    aws_secret_access_key = <AWS_SECRET_ACCESS_KEY>
    ```
    - Update the configuration and save the file.

## Run the Greengrass container
- Run the Greengrass container with the following command:
```sh
docker run --init -it --name aws-iot-greengrass \
-v <Path_to_folder_having_AWS_credentials_file>:/root/.aws/:ro \
--env-file <Path_to_.env_file> \
-p 8883 \
<IMAGE_NAME>:<TAG_NAME>
```
This will launch the Greengrass service and register the device as a Greengrass core device. After the Greengrass nucleus has been launched, you can see your device in the AWS IoT console under **core devices** section in the Greengrass console.

## Copy the lambda function and launch it inside the container
- Open a new terminal and navigate to the Greengrass folder.
```sh
cd <PROJECT_ROOT>/Greengrass
```
- Open the **greengrassv2/artifacts/com.example.lambda/1.0.0/lambda.py** file in an editor:
    - Line number 9 and 10, replace the <DEVICE_NAME>.
    - Line number 11, speciy <CLOUDWATCH_NAMESPACE>.

        Note that while configuring cloudwatch dashboard, this namespace should be used to select metrics.
    - Line number 12, 13, 14 update <AWS_REGION>, <AWS_ACCESS_KEY_ID> and <AWS_SECRET_KEY>.
- Open the **greengrassv2/recipes/com.example.lambda-1.0.0.json** file in an editor:
    - Line number 18 and 19, update the <DEVICE_NAME>.

        Note that the Greengrass container will receive the detection stats through the topic “<DEVICE_NAME>/stats” and publish it on “<DEVICE_NAME>/anl_res” topic. This same topic should be given in the config file of the cloud container (csp-amazon-web-services > Cloud_container > config).
- Copy the **greengrassv2** folder into the running greengrass container.
```sh
docker cp greengrassv2 aws-iot-greengrass:/root/
```
- Create a bash inside the running greengrass container.
```sh
docker exec –it aws-iot-greengrass bash
```
- Check if the executable **greengrass-cli** has been created in **/greengrass/v2/bin/** folder.

    Note that the executable can take some time to create. If you do not find it, check that your container is connected to the thing group previously created with **aws.greengrass.Cli** and other components.
- After the executable is created, deploy the lambda function using the following command in the running container.

```sh
sudo /greengrass/v2/bin/greengrass-cli deployment create \
--recipeDir ~/greengrassv2/recipes \
--artifactDir ~/greengrassv2/artifacts \
--merge "com.example.lambda=1.0.0"
```
- Check if the component has been created or not using the following command.
```sh
sudo /greengrass/v2/bin/greengrass-cli component list
```
- If you don’t see the component with the name “com.example.lambda”, deploy the lambda function again.
- To see if the component has started, we can see the logs of the component.
```sh
cat /greengrass/v2/logs/com.example.lambda.log
```

## Download certificates for device
When the greengrass container is launched, it downloads certificates which are used to access the AWS IoT core service. We need to copy these certificates to the local device for building the CSP container (csp-amazon-web-services > Cloud_container) and the rebooot service.

- Open a new terminal and login to the edge device using ssh.
- Create a new folder for the certificates in the below format.
    ```sh
    mkdir device_certs/
    ```

    Note that this certificate folder will be used when building the CSP container.
- Copy the thing certificate and the private key from the running greengrass container.
```sh
docker cp aws-iot-greengrass:/greengrass/v2/thingCert.crt device_certs/device.pem.crt
docker cp aws-iot-greengrass:/greengrass/v2/privKey.key device_certs/device-private.pem.key
```
- Download the root ca certificate and rename it using below commands.
```sh
wget https://www.amazontrust.com/repository/AmazonRootCA1.pem
mv AmazonRootCA1.pem device_certs/root.ca.pem
```

## Save the container for future use
To use the Greengrass service as configured above in the future, we can save it.
```sh
docker commit aws-iot-greengrass <IMAGE_NAME>:<IMAGE_TAG>
```

## Push the container to Docker hub
To use the Greengrass container with Kubernetes, we need to push the image to docker hub.
- List the docker images.
```sh
docker images
```
- Tag the greengrass image.
```sh
docker tag <greengrass_IMAGE_ID> <docker_hub_username>/<IMAGE_NAME>:<IMAGE_TAG>
```
- Make sure you are logged in with your docker hub account and push the image.
```sh
docker login
docker push <docker_hub_username>/<IMAGE_NAME>
```

## Issues with IAM role
While running the Greengrass container if the user gets the following error with IAM role, follow the further steps to set up a custom IAM role.
```
User: <ARN_OF_USER> is not authorized to perform: iam:CreatePolicy on resource: policy GreengrassAccess because no identity-based policy allows the iam:CreatePolicy action.
```
The Greengrass container creates an execution role and creates an alias by default. If the user wants to use a custom IAM role, follow the below steps.

- Go to AWS cloud console.

1. IAM Role creation.
    - search for **IAM role** and access this service.
    - On the left side menu click on **Roles** under **Access management**.
    - Click on **Create role**. Then:
        - In **Trusted entity type**, select **Custom trust policy**.
        - Replace the contents of the editor with the following:
        ```
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "credentials.iot.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
        ```
        - Click on **Next**. On the next window, search for **AmazonS3FullAccess** in the search bar, select the policy and click on **Next**.
        - On the next window provide the role name of the user choice and click on **Create role**.
2. IAM Policy creation.
    - On the left side menu click on **Policies** and click on **Create policy**. Then, select the **json** tab and replace the content of the editor with the following.
        ```
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "logs:CreateLogGroup",
                        "logs:CreateLogStream",
                        "logs:PutLogEvents",
                        "logs:DescribeLogStreams",
                        "s3:GetBucketLocation"
                    ],
                    "Resource": "*"
                }
            ]
        }
        ```
    - Click on **Next: Tags** and then again on **Next: Review** to access the review page.
    - For the policy name, provide one in the following format "<ROLE_NAME>Access". For example if the role name is Greengrass, the policy name should be "GreengrassAccess".
    - Click on **Create policy**.

3. Attach the policy to the role.
    -  On the left side menu click on **Roles** and search for the one created previously. Click on the role.
    - In the **Permissions** tab, click on **Add permissions** and select **Attach policies** from the dropdown menu.
    - Search for the policy created in the last step and select it.
    - Click on **Add permissions**.
4. Create an alias in AWS IoT Core.
    - Search for **IoT core** on the AWS console and access this service.
    - On the left side menu under **Security** section, select **Role Aliases**.
    - Click on **Create role alias**.
    - Provide an alias name in the following format "<IAM_ROLE_NAME>Alias". For example, if the IAM role name is Greengrass, the alias name should be "GreengrassAlias".
    - In the **Role** dropdown, select the IAM role which was created previously.
    - Click on **Create**.

    The IAM role and Role Alias created can now be specified in the environment file ( .env file) which is given when launching the Greengrass container. Change the following parameters in env file:
    ```
    TES_ROLE_NAME=<IAM_ROLE_NAME>
    TES_ROLE_ALIAS_NAME=<ROLE_ALIAS>
    ```

## Issues with Greengrass Deployment
If you are using the configured thing group and default deployment and the deployment to Greengrass core device is not working. Then revise the deployment once.

## References
- https://docs.aws.amazon.com/greengrass/v2/developerguide/build-greengrass-dockerfile.html
- https://docs.aws.amazon.com/greengrass/v2/developerguide/run-greengrass-docker-automatic-provisioning.html
- https://docs.aws.amazon.com/greengrass/v2/developerguide/device-service-role.html