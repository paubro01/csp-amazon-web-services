import time
import traceback
import json
import boto3
import awsiot.greengrasscoreipc
import awsiot.greengrasscoreipc.client as client
from awsiot.greengrasscoreipc.model import (IoTCoreMessage, QOS, PublishToIoTCoreRequest, SubscribeToIoTCoreRequest)

subscribetopic = "<DEVICE_NAME>/stats"
pub_topic = "<DEVICE_NAME>/anl_res"
cloudwatch_namespace = "<CLOUDWATCH_NAMESPACE>"
aws_region = "<AWS_REGION>"
aws_access_key_id = "<AWS_ACCESS_KEY_ID>"
aws_secret_access_key = "<AWS_SECRET_KEY>"


TIMEOUT = 1
qos = QOS.AT_LEAST_ONCE
subqos = QOS.AT_MOST_ONCE

cloudwatch = boto3.client("cloudwatch", region_name=aws_region,                         
                                        aws_access_key_id=aws_access_key_id,                                                 
                                        aws_secret_access_key=aws_secret_access_key)

ipc_client = awsiot.greengrasscoreipc.connect()

class SubHandler(client.SubscribeToIoTCoreStreamHandler):
    def __init__(self):
        super().__init__()

    def on_stream_event(self, event: IoTCoreMessage) -> None:
        try:
            message = str(event.message.payload, "utf-8")
            topic_name = event.message.topic_name
            # Handle message.
            jsonmsg = json.loads(message)

            #post metrics to cloudwatch
            if jsonmsg["post_cloudwatch"] == "True":
                cloudwatch.put_metric_data(                                                                                                   
                                MetricData = [ {"MetricName": "Person", "Unit":"None", "Value" : jsonmsg["person"]},                      
                                               {"MetricName": "Bicycle", "Unit":"None", "Value" : jsonmsg["bicycle"]},                         
                                               {"MetricName": "Car", "Unit":"None", "Value": jsonmsg["car"]},                               
                                               {"MetricName": "Bus", "Unit":"None", "Value": jsonmsg["bus"]},                             
                                               {"MetricName": "Truck", "Unit":"None", "Value": jsonmsg["truck"]},                         
                                               {"MetricName": "End_to_End_FPS", "Unit":"None", "Value": jsonmsg["End_to_end_FPS"]},      
                                               {"MetricName": "Pure_FPS", "Unit":"None", "Value": jsonmsg["Pure_inf"]}],       
                                               Namespace = cloudwatch_namespace)
                print("Sent metrics to cloudwatch")

            #Analyze the metrics received and post analysis result 
            obj = int(jsonmsg["total_count"])
            if obj > 10:
                payload = "High traffic detected...!!!"
            elif obj <= 10 and obj > 5:
                payload = "Medium traffic detected..!!"
            else :
                payload = "Low traffic detected.!"

            msg = json.dumps({"analysis_result": payload})
            
            pubrequest = PublishToIoTCoreRequest()      
            pubrequest.topic_name = pub_topic
            pubrequest.payload = bytes(msg, "utf-8")
            pubrequest.qos = qos
            operation = ipc_client.new_publish_to_iot_core()
            operation.activate(pubrequest)
            future = operation.get_response()
            #future.result(TIMEOUT)         
        
        except:
            pass

    def on_stream_error(self, error: Exception) -> bool:
        # Handle error.
        return True  # Return True to close stream, False to keep stream open.

    def on_stream_closed(self) -> None:
        # Handle close.
        pass

subrequest = SubscribeToIoTCoreRequest()
subrequest.topic_name = subscribetopic
subrequest.qos = subqos
handler = SubHandler()
operation = ipc_client.new_subscribe_to_iot_core(handler)
future = operation.activate(subrequest)
future.result(TIMEOUT)

while True:
    pass
