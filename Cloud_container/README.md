# Building and Deploying the Cloud Container

## Introduction
This document guides the user to build and deploy the cloud container.

## Pre-Requisites
- AWS Greengrass container up and running.
- AWS IoT device certificates downloaded from the Greengrass container.
- AWS account with services IoT core, Cloudwatch and lambda enabled.

## Building the Cloud container
Follow the steps given below for setting up the container.
- Open a terminal on the host system and connect to the Edge device using SSH.
- Navigate to the root of the project folder.
  ```sh
  cd <PROJECT_ROOT_FOLDER>/Cloud_container/
  ```
- Copy the device certificates folder obtained from the Greengrass container into the current directory.

  NB: refer to <PROJECT_ROOT_FOLDER>/Greengrass/REAME.md for launching the greengrass container and getting the device certificates.
  ```sh
  cp <PATH_TO_DEVICE_CERTIFICATES_FOLDER> .
  ```
- Open the config file located at **./config/service_config.json** in an editor. Then:
  - Verify that the "aws_iot_cert_folder" attribute value corresponds to the device certificates folder name previously copied.
  - For the "gg_host" (Greengrass host) attribute, we need the AWS IoT Device data endpoint (<AWS_IOT_ENDPOINT>).
    - Go to the AWS console.
    - Search and select **IoT Core** service.
    - From the top bar, make sure you are still situated in the same region as the one chosen to set up the Greengrass container.
    - On the left side menu, select **Settings**. Under **Device data endpoint**, copy the endpoint URL and update the "gg_host" value <AWS_IOT_ENDPOINT> in the local config file.
  - Enter the value of the mqtt publish topic ("pub_topic") <DEVICE_NAME>.

    Note that this should be the same as the one given during the building of the Greengrass container (see “<DEVICE_NAME>/stats” and “<DEVICE_NAME>/anl_res” in <PROJECT_ROOT_FOLDER>/Greengrass/REAME.md).
  - Save the config file.


- Run the command given below for building the cloud container.
  ```sh
  docker build -t <IMAGE_NAME>:<TAG_NAME> .
  ```

  Mention the image name & tag name of your choice.
- Check the Docker Images list with the command below.
  ```sh
  docker images
  ```

## Deploy the Cloud container
- Deploy the cloud container using the command below
  ```sh
  docker run --network=host <IMAGE_NAME>:<TAG_NAME> \
              -p <CLOUD_CONTAINER_PORT> \
              -cw_i <CLOUDWATCH_MESSAGE_POSTING_INTERVAL>
  ```
  Arguments :
  - -p: Port number on which the flask server should be running.
    - This should be different from the inference container port and application container flask server port.
  - -cw_i: Cloudwatch message posting interval. Given in seconds. Default value is 60 seconds.


## What's next ?
The Cloudwatch folder's ReadMe to set up the corresponding service.