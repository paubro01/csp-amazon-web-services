
def ArgConf(ap):
    ap.add_argument("-p", "--cloud_port", default="8081", help="Cloud container Flask server port number") 
    ap.add_argument("-cw_i", "--cloudwatch_post_interval", default=60, help="Time interval for cloudwatch message posting") 
    args = vars(ap.parse_args())
    return args