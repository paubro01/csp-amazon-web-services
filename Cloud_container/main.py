import json
import argparse
import time
from flask import Flask, request, redirect, flash, jsonify
from src.argconfig import ArgConf
from src.cloud_agent import AwsAgent

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)

cloud_agent = AwsAgent(args["cloudwatch_post_interval"])

@app.route('/video', methods=['POST'])
def text():
    if request.method == 'POST':
        cloud_agent.send_metrics_to_cloud(request.json['cloud_metrics'])
        end_rep = time.time()
        return jsonify({"message" : "Sent data to cloud", "End_to_End_pipeline_time": end_rep})

if __name__ == '__main__': 
    app.run(host="0.0.0.0",port=args['cloud_port'])

